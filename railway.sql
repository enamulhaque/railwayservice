-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2018 at 06:44 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `railway`
--

-- --------------------------------------------------------

--
-- Table structure for table `p_info`
--

CREATE TABLE `p_info` (
  `id` int(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phn_num` bigint(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_info`
--

INSERT INTO `p_info` (`id`, `name`, `email`, `password`, `address`, `phn_num`) VALUES
(1, 'khairul hasib', 'dipto3417@gmail.com', '22', 'rampura', 1526998875),
(2, 'hasibul hasan', 'hasibul@gmail.com', '12345', 'bansree', 1825056633),
(3, 'Rayan', 'rayan55@gmail.com', '222', 'badda', 1755693325),
(7, 'abba', 'a@a', 'b', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `train_info`
--

CREATE TABLE `train_info` (
  `id` int(6) NOT NULL,
  `train_name` varchar(20) NOT NULL,
  `t_from` varchar(20) NOT NULL,
  `t_to` varchar(20) NOT NULL,
  `starting_time` time DEFAULT NULL,
  `araival_time` time DEFAULT NULL,
  `dayoff` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `train_info`
--

INSERT INTO `train_info` (`id`, `train_name`, `t_from`, `t_to`, `starting_time`, `araival_time`, `dayoff`) VALUES
(2001, 'akota', 'dhaka', 'dinajpur', '08:30:00', '20:30:00', 'sunday'),
(2002, 'sundorban', 'dhaka', 'khulna', '08:30:00', '22:30:00', 'saturday'),
(2003, 'padma', 'dhaka', 'rajshahi', '06:30:00', '12:30:00', 'sunday'),
(2004, 'silk sity', 'dhaka', 'rajshahi', '02:45:00', '10:30:00', 'sunday');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p_info`
--
ALTER TABLE `p_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `train_info`
--
ALTER TABLE `train_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p_info`
--
ALTER TABLE `p_info`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `train_info`
--
ALTER TABLE `train_info`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2005;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
