-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2018 at 04:12 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `railway`
--

-- --------------------------------------------------------

--
-- Table structure for table `date`
--

CREATE TABLE `date` (
  `t_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `seat_taken` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `date`
--

INSERT INTO `date` (`t_id`, `date`, `seat_taken`) VALUES
(101, '2018-08-02', 92),
(101, '2018-08-03', 13),
(201, '2018-08-02', 7),
(401, '2018-08-02', 7),
(301, '2018-08-02', 7),
(201, '2018-08-04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `padma`
--

CREATE TABLE `padma` (
  `t_name` varchar(20) NOT NULL,
  `t_from` varchar(20) NOT NULL,
  `t_to` varchar(20) NOT NULL,
  `distance` varchar(10) NOT NULL,
  `s_chair` varchar(10) NOT NULL,
  `ac_sit` varchar(10) NOT NULL,
  `ac_berth` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `padma`
--

INSERT INTO `padma` (`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`) VALUES
('padma', 'dhaka', 'joydebpur', '40km', '80tk', '150tk', '300tk'),
('padma', 'dhaka', 'tangail', '80km', '120tk', '250tk', '500tk'),
('padma', 'dhaka', 'monsur_ali', '120km', '220tk', '350tk', '700tk'),
('padma', 'dhaka', 'sirajgang', '180km', '310tk', '450tk', '950tk'),
('padma', 'dhaka', 'ullapara', '220km', '325tk', '500tk', '1000tk'),
('padma', 'dhaka', 'boral_brize', '250km', '350tk', '550tk', '1050tk'),
('padma', 'dhaka', 'ishurdi', '271km', '385tk', '620tk', '1280tk'),
('padma', 'dhaka', 'chuadanga', '300km', '480tk', '950tk', '1500tk'),
('padma', 'dhaka', 'rajbari', '320km', '520tk', '1150tk', '1800tk'),
('padma', 'dhaka', 'jessore', '340km', '545tk', '1250tk', '1900tk'),
('padma', 'dhaka', 'khulna', '388km', '675tk', '1380tk', '2120tk');

-- --------------------------------------------------------

--
-- Table structure for table `p_info`
--

CREATE TABLE `p_info` (
  `id` int(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phn_num` bigint(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_info`
--

INSERT INTO `p_info` (`id`, `name`, `email`, `password`, `address`, `phn_num`) VALUES
(1, 'khairul hasib', 'dipto3417@gmail.com', '22', 'rampura', 1526998875),
(2, 'hasibul hasan', 'hasibul@gmail.com', '12345', 'bansree', 1825056633),
(3, 'Rayan', 'rayan55@gmail.com', '222', 'badda', 1755693325),
(7, 'abba', 'a@a.a', 'a', '', 0),
(9, 'rajib', 'b@b.b', '2', 'gulshan', 1766220011),
(10, 'hasibl hasan', 'c@c.c', '55', 'tongi', 1562003388),
(11, 'nahin', 'nahinhasan22@gmail.c', '223', 'ishurdi,pabna', 1525636950),
(12, 'rakib', 'rakibhasan55@gmailco', '3399', '', 0),
(13, 'rakib', 'rakibhasan55@gmailco', '3399', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `seat`
--

CREATE TABLE `seat` (
  `t_id` int(10) NOT NULL,
  `seat` int(10) NOT NULL,
  `section` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seat`
--

INSERT INTO `seat` (`t_id`, `seat`, `section`) VALUES
(101, 200, 4),
(401, 400, 5),
(301, 500, 4),
(201, 600, 6);

-- --------------------------------------------------------

--
-- Table structure for table `sundorban`
--

CREATE TABLE `sundorban` (
  `t_name` varchar(20) NOT NULL,
  `t_from` varchar(20) NOT NULL,
  `t_to` varchar(20) NOT NULL,
  `distance` varchar(20) NOT NULL,
  `s_chair` varchar(10) NOT NULL,
  `ac_sit` varchar(10) NOT NULL,
  `ac_berth` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sundorban`
--

INSERT INTO `sundorban` (`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`) VALUES
('sundorban', 'dhaka', 'joydebpur', '40km', '80tk', '150tk', '300tk'),
('sundorban', 'dhaka', 'tangail', '80km', '120tk', '250tk', '500tk'),
('sundorban', 'dhaka', 'monsur_ali', '120km', '220tk', '350tk', '700tk'),
('sundorban', 'dhaka', 'sirajgang', '180km', '310tk', '450tk', '950tk'),
('sundorban', 'dhaka', 'ullapara', '220km', '325tk', '500tk', '1000tk'),
('sundorban', 'dhaka', 'boral_brize', '250km', '350tk', '550tk', '1050tk'),
('sundorban', 'dhaka', 'ishurdi', '271km', '385tk', '620tk', '1280tk'),
('sundorban', 'dhaka', 'chuadanga', '300km', '480tk', '950tk', '1500tk'),
('sundorban', 'dhaka', 'rajbari', '320km', '520tk', '1150tk', '1800tk'),
('sundorban', 'dhaka', 'jessore', '340km', '545tk', '1250tk', '1900tk'),
('sundorban', 'dhaka', 'khulna', '388km', '675tk', '1380tk', '2120tk');

-- --------------------------------------------------------

--
-- Table structure for table `train_cost`
--

CREATE TABLE `train_cost` (
  `t_id` int(10) NOT NULL,
  `t_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `train_cost`
--

INSERT INTO `train_cost` (`t_id`, `t_name`) VALUES
(2002, 'sundorban'),
(2001, 'dhumketu'),
(2003, 'chitra'),
(2004, 'silk_city'),
(2005, 'padma'),
(2006, 'akota');

-- --------------------------------------------------------

--
-- Table structure for table `train_info`
--

CREATE TABLE `train_info` (
  `t_id` int(6) NOT NULL,
  `train_name` varchar(20) NOT NULL,
  `t_from` varchar(20) NOT NULL,
  `t_to` varchar(20) NOT NULL,
  `starting_time` time DEFAULT NULL,
  `araival_time` time DEFAULT NULL,
  `dayoff` varchar(10) DEFAULT NULL,
  `fare` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `train_info`
--

INSERT INTO `train_info` (`t_id`, `train_name`, `t_from`, `t_to`, `starting_time`, `araival_time`, `dayoff`, `fare`) VALUES
(101, 'akota', 'dhaka', 'dinajpur', '08:30:00', '20:30:00', 'sunday', 500),
(201, 'sundorban', 'dhaka', 'khulna', '08:30:00', '22:30:00', 'saturday', 600),
(301, 'padma', 'dhaka', 'rajshahi', '06:30:00', '12:30:00', 'sunday', 400),
(401, 'silk sity', 'dhaka', 'rajshahi', '02:45:00', '10:30:00', 'sunday', 700);

-- --------------------------------------------------------

--
-- Table structure for table `train_time`
--

CREATE TABLE `train_time` (
  `t_name` varchar(15) NOT NULL,
  `t_from` varchar(20) NOT NULL,
  `t_to` varchar(20) NOT NULL,
  `s_time` time(6) NOT NULL,
  `a_time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `train_time`
--

INSERT INTO `train_time` (`t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES
('sundorban', 'dhaka', 'khulna', '08:30:00.000000', '22:30:00.000000'),
('sundorban', 'dhaka', 'jessore', '08:30:00.000000', '20:30:00.000000'),
('sundorban', 'dhaka', 'chuadanga', '08:30:00.000000', '19:30:00.000000'),
('sundorban', 'dhaka', 'kustia', '08:30:00.000000', '18:10:00.000000'),
('sundorban', 'dhaka', 'khulna', '08:30:00.000000', '17:30:00.000000'),
('sundorban', 'dhaka', 'rajbari', '08:30:00.000000', '16:30:00.000000'),
('sundorban', 'dhaka', 'ishurdi', '08:30:00.000000', '15:30:00.000000'),
('sundorban', 'dhaka', 'chatmohor', '08:30:00.000000', '14:50:00.000000'),
('sundorban', 'dhaka', 'ullapara', '08:30:00.000000', '13:20:00.000000'),
('sundorban', 'dhaka', 'mirjapur', '08:30:00.000000', '12:30:00.000000'),
('sundorban', 'dhaka', 'tangail', '08:30:00.000000', '11:10:00.000000'),
('sundorban', 'dhaka', 'tongi', '08:30:00.000000', '10:40:00.000000'),
('sundorban', 'dhaka', 'airport', '08:30:00.000000', '09:50:00.000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `date`
--
ALTER TABLE `date`
  ADD KEY `t_id` (`t_id`);

--
-- Indexes for table `p_info`
--
ALTER TABLE `p_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seat`
--
ALTER TABLE `seat`
  ADD UNIQUE KEY `seat` (`seat`),
  ADD KEY `t_id` (`t_id`);

--
-- Indexes for table `train_info`
--
ALTER TABLE `train_info`
  ADD PRIMARY KEY (`t_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p_info`
--
ALTER TABLE `p_info`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `train_info`
--
ALTER TABLE `train_info`
  MODIFY `t_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=402;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `date`
--
ALTER TABLE `date`
  ADD CONSTRAINT `date_ibfk_1` FOREIGN KEY (`t_id`) REFERENCES `seat` (`t_id`);

--
-- Constraints for table `seat`
--
ALTER TABLE `seat`
  ADD CONSTRAINT `seat_ibfk_1` FOREIGN KEY (`t_id`) REFERENCES `train_info` (`t_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
