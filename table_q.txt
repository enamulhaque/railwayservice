create table train_info ( id int(6) auto_increment primary key,
                                 train_name varchar(20) not null,
                                 t_from varchar(20) not null,
                                 t_to varchar (20) not null,
                         		starting_time time,
               				araival_time time,
                         		dayoff varchar(10)
                                   );

INSERT INTO `train_info`(`id`, `train_name`, `t_from`, `t_to`, `starting_time`, `araival_time`, `dayoff`) VALUES (2002,'sundorban','dhaka','khulna','08:30:00','22:30:00','saturday');


INSERT INTO `train_cost`(`t_id`, `t_name`)VALUES (2001,'dhumketu');
INSERT INTO `train_cost`(`t_id`, `t_name`)VALUES (2002,'sundorban');
INSERT INTO `train_cost`(`t_id`, `t_name`)VALUES (2003,'chitra');
INSERT INTO `train_cost`(`t_id`, `t_name`)VALUES (2004,'silk_city');
INSERT INTO `train_cost`(`t_id`, `t_name`)VALUES (2005,'padma');
INSERT INTO `train_cost`(`t_id`, `t_name`)VALUES (2006,'akota');


INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`)
 VALUES ('padma','dhaka','joydebpur','40km','80tk','150tk','300tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`)
 VALUES ('padma','dhaka','tangail','80km','120tk','250tk','500tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`)
VALUES ('padma','dhaka','monsur_ali','120km','220tk','350tk','700tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`)
VALUES ('padma','dhaka','sirajgang','180km','310tk','450tk','950tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`)
 VALUES ('padma','dhaka','ullapara','220km','325tk','500tk','1000tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`) VALUES ('padma','dhaka','boral_brize','250km','350tk','550tk','1050tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`) VALUES ('padma','dhaka','ishurdi','271km','385tk','620tk','1280tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`) VALUES ('padma','dhaka','chuadanga','300km','480tk','950tk','1500tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`) VALUES ('padma','dhaka','rajbari','320km','520tk','1150tk','1800tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`) VALUES ('padma','dhaka','jessore','340km','545tk','1250tk','1900tk');
INSERT INTO `padma`(`t_name`, `t_from`, `t_to`, `distance`, `s_chair`, `ac_sit`, `ac_berth`) VALUES ('padma','dhaka','khulna','388km','675tk','1380tk','2120tk');


INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','khulna','08:30:00','22:30:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','jessore','08:30:00','20:30:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','chuadanga','08:30:00','19:30:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','kustia','08:30:00','18:10:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','khulna','08:30:00','17:30:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','rajbari','08:30:00','16:30:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','ishurdi','08:30:00','15:30:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','chatmohor','08:30:00','14:50:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','ullapara','08:30:00','13:20:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','mirjapur','08:30:00','12:30:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','tangail','08:30:00','11:10:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','tongi','08:30:00','10:40:00');
INSERT INTO `train_time`( `t_name`, `t_from`, `t_to`, `s_time`, `a_time`) VALUES ('sundorban','dhaka','airport','08:30:00','9:50:00');
